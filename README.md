# Angular2TodoApp

esta aplicacion esta basada en 
https://www.sitepoint.com/angular-2-tutorial/

## 1) creando la base de la app  
#### instala en angular-cli si aun no lo tienes 
```
npm install -g angular-cli
```
#### crea una nueva aplicacion
```
ng new angular2-todo-app
```

#### cambiate a ese directorio 
```
cd angular2-todo-app
```

#### ejecuta la aplicacion para ver que todo a ido bien 
```
ng serve
```


#### actualiza la version del package.json 
```
  "version": "0.0.1",
```

#### crea en bitbucket un repositorio nuevo y vinculqalo 
```
git remote add origin https://guzmanator@bitbucket.org/guzmanator/angular2-todo-app.git
git push -u origin master
```
#### sube la version 
```
git add .
git commit -m "initial commit"
git push origin master
git tag 0.0.1
git push origin 0.0.1
```

## 2) creando la clase todo 

#### ejecutamos el comando para crear un nuevo componente con el cli 
```
ng generate class Todo --spec
```

#### esto generara los ficheros del componente 
```
src/app/todo.spec.ts
src/app/todo.ts
```

#### cambiamos el contenido de todo.js para que tenga una propiedad id, un titulo y un flag para saber si esta completado, tambien en el constructor permitimos que nos definan otras propiedades que necesitemos  
``` javascript 
export class Todo {
  id: number;
  title: string = '';
  complete: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
```

#### de esta forma se puede llamar al constructor pasando los valores 
```javascript
let todo = new Todo({
  title: 'Read SitePoint article',
  complete: false
});
```

#### rellenamos el test minimo para ver que nuestra clase funciona correctamente 
```javascript
import {Todo} from './todo';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new Todo()).toBeTruthy();
  });
  it('should accept values in the constructor', () => {
    let todo = new Todo({
      title: 'hello',
      complete: true
    });
    expect(todo.title).toEqual('hello');
    expect(todo.complete).toEqual(true);
  });
});
```

#### actualiza la version del package.json 
```
  "version": "0.0.2",
```

#### sube la version actual a git
```
git add .
git commit -m "added todo class"
git push origin master
git tag 0.0.2
git push origin 0.0.2
```

## 3) creando un servicio 

#### el TodoDataService sera el responsable de manejar nuestros todos 

#### mas adelante llamaremos a un api rest, pero por ahora lo almacenaremos en memoria local 

Let’s use Angular CLI again to generate the service for us:




#### podemos crear un servicio con el comando del cli 
```
ng generate service TodoData
```

#### abrimos src/app/todo-data.service.ts y añadimos la logica a nuestro TodoDataService:
``` javascript
import {Injectable} from '@angular/core';
// importamos la clase todo que creamos antes 
import {Todo} from './todo';

// convertimos nuestro servicio en inyectable de forma que pueda ser usado por DI en nuestros componentes 
@Injectable()

// exportamos nuestro servicio como una clase 
export class TodoDataService {

  // Placeholder para el ultimo id asi podemos simular
  // incremento automatico de ids 
  lastId: number = 0;

  // array de todos para guradar los introducido por
  // el usuario 
  todos: Todo[] = [];

  constructor() {
  }

  // Simulate POST /todos
  addTodo(todo: Todo): TodoDataService {
    // si no hay id en el todo que nos pasan utilizamos 
    // el autogenerado 
    if (!todo.id) {
      todo.id = ++this.lastId;
    }
    // incluimos el todo 
    // notese que como no usamos el id 
    // no podemos editar con este metodo
    this.todos.push(todo);
    return this;
  }

  // Simulate DELETE /todos/:id
  deleteTodoById(id: number): TodoDataService {
    // recorremos los todos y los devolvemos salvo que el id conicida con el que nos pasan 
    // notese que filter devuelve un nuevo array y pisamos el anterior
    this.todos = this.todos
      .filter(todo => todo.id !== id);
    return this;
  }

  // Simulate PUT /todos/:id
  updateTodoById(id: number, values: Object = {}): Todo {
    // recuperamos el todo por id 
    let todo = this.getTodoById(id);
    // si no hay ninguno con ese id salimos 
    if (!todo) {
      return null;
    }
    // sobreescribimos el todo con los valores que nos pasan
    // notese que como nos quedamos con la referencia al objeto modificamos el que esta en el array no una copia 
    Object.assign(todo, values);
    // devolvemos el todo modificado para segimiento
    return todo;
  }

  // Simulate GET /todos
  getAllTodos(): Todo[] {
    // devolvemos los todos
    // notese que es una referencia, puede usares para modificar 
    return this.todos;
  }

  // Simulate GET /todos/:id
  getTodoById(id: number): Todo {
    // recorremos el array para encontrar el que tiene el id 
    // notese que filter devuelve un array como queremos el elemento y por referencia usamos pop para sacarlo 
    // notese que no controlamos el caso de no hay ¿se devuelve null?
    return this.todos
      .filter(todo => todo.id === id)
      .pop();
  }

  // Toggle todo complete
  toggleTodoComplete(todo: Todo){
    // llamamos a la funcion de update pisando el valor de complete
    // notese que vale para marcar o desmarcar
    let updatedTodo = this.updateTodoById(todo.id, {
      complete: !todo.complete
    });
    // como el metodo devuelve el modificado lo retornamos para encadenado
    return updatedTodo;
  }

}
```

#### hacemos los test 
```javascript
import {TestBed, async, inject} from '@angular/core/testing';
import {Todo} from './todo';
import {TodoDataService} from './todo-data.service';

describe('TodoDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoDataService]
    });
  });

  it('should ...', inject([TodoDataService], (service: TodoDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAllTodos()', () => {

    it('should return an empty array by default', inject([TodoDataService], (service: TodoDataService) => {
      expect(service.getAllTodos()).toEqual([]);
    }));

    it('should return all todos', inject([TodoDataService], (service: TodoDataService) => {
      let todo1 = new Todo({title: 'Hello 1', complete: false});
      let todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
    }));

  });

  describe('#save(todo)', () => {

    it('should automatically assign an incrementing id', inject([TodoDataService], (service: TodoDataService) => {
      let todo1 = new Todo({title: 'Hello 1', complete: false});
      let todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getTodoById(1)).toEqual(todo1);
      expect(service.getTodoById(2)).toEqual(todo2);
    }));

  });

  describe('#deleteTodoById(id)', () => {

    it('should remove todo with the corresponding id', inject([TodoDataService], (service: TodoDataService) => {
      let todo1 = new Todo({title: 'Hello 1', complete: false});
      let todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
      service.deleteTodoById(1);
      expect(service.getAllTodos()).toEqual([todo2]);
      service.deleteTodoById(2);
      expect(service.getAllTodos()).toEqual([]);
    }));

    it('should not removing anything if todo with corresponding id is not found', inject([TodoDataService], (service: TodoDataService) => {
      let todo1 = new Todo({title: 'Hello 1', complete: false});
      let todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
      service.deleteTodoById(3);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
    }));

  });

  describe('#updateTodoById(id, values)', () => {

    it('should return todo with the corresponding id and updated data', inject([TodoDataService], (service: TodoDataService) => {
      let todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      let updatedTodo = service.updateTodoById(1, {
        title: 'new title'
      });
      expect(updatedTodo.title).toEqual('new title');
    }));

    it('should return null if todo is not found', inject([TodoDataService], (service: TodoDataService) => {
      let todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      let updatedTodo = service.updateTodoById(2, {
        title: 'new title'
      });
      expect(updatedTodo).toEqual(null);
    }));

  });

  describe('#toggleTodoComplete(todo)', () => {

    it('should return the updated todo with inverse complete status', inject([TodoDataService], (service: TodoDataService) => {
      let todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      let updatedTodo = service.toggleTodoComplete(todo);
      expect(updatedTodo.complete).toEqual(true);
      service.toggleTodoComplete(todo);
      expect(updatedTodo.complete).toEqual(false);
    }));

  });

});
```




Let’s zoom in on some of the parts in the unit tests above:
#### veamos algunas de las partes mas nuevas de los test 
```javascript
beforeEach(() => {
  TestBed.configureTestingModule({
    providers: [TodoDataService]
  });
});
```

#### TestBed es una utilidad que da el core de angular para configurar modulkos de angular dentro de nuestros test 

#### utilizamos el metodo TestBed.configureTestingModule() para comfigurar y crear el modulo de nuestro servicio, y nos ahorrmos toda la parte de bootstraping que se necesita en una aplicacion angular 2

#### en este caso solamente necesitamos la parte de los provehedores para ejecutar lso test


#### usamos la palabra clave inyect parta poner nuestro servicio en los tests individuales 
```javascript
it('should return all todos', inject([TodoDataService], (service: TodoDataService) => {
  let todo1 = new Todo({title: 'Hello 1', complete: false});
  let todo2 = new Todo({title: 'Hello 2', complete: true});
  service.addTodo(todo1);
  service.addTodo(todo2);
  expect(service.getAllTodos()).toEqual([todo1, todo2]);
}));
```

#### probamos que todo funciona bien 
```
ng test
```


#### actualiza la version del package.json 
```
  "version": "0.0.3",
```

#### sube la version actual a git
```
git add .
git commit -m "added todo service"
git push origin master
git tag 0.0.3
git push origin 0.0.3
```
